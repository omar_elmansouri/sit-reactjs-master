const ProductFiltersData = [
  { name: 'Nut Free' },
  { name: 'Vegan' },
  { name: 'Vegetarian' },
  { name: 'Diary Free' },
  { name: 'Gluten Free' },
  { name: 'Low Carb' },
]

export default ProductFiltersData
