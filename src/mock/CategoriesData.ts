
import { withTranslation, WithTranslation } from "react-i18next";
interface Props extends WithTranslation  {

}

const CategoriesData = [
  { name: 'Special Deals' },
  { name: 'Pizza' },
  { name: 'Nibbles' },
  { name: 'Share Platters' },
  { name: 'Pasta' },
  { name: 'Burgers & Sandwiches' },
  { name: 'From the Grill' },
  { name: 'Sides' },
  { name: 'Sweet Ending' },
  { name: 'On Tap' },
  { name: 'Bottled Drinks' },
  { name: 'Wine' },
  { name: 'Spirits' },
  { name: 'Soft Drinks & Juice' },
  { name: 'Beverages' },
  
]

export default CategoriesData
