import { types, LoginState } from "./types";

const initialState: LoginState = {
  token: undefined,
  loggedInStatus: false,
  logOutStatus: false,
  errorMessage: "",
  checkOutRegister: false,
  loading: false,
  changePassword: false,
  updateUser: false,
  loadingCheckout: false,

}

const userReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case types.REGISTER_REQUEST:
      return {
        ...state,
        loggedInStatus: false,
      };
    case types.REGISTER_SUCCESS:
      return {
        ...state,
        loggedInStatus: true,
        logOutStatus: false,
        errorMessage: ""
      };
    case types.REGISTER_FAILURE:
      return {
        ...state,
        errorMessage: action.payload
      };
    case types.LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        loggedInStatus: false,
        errorMessage: ""
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload,
        loggedInStatus: true,
        logOutStatus: false,
        errorMessage: "",
        loading: false
      };
    case types.USER_INFO_SUCCESS:
      return {
        ...state,
        userInfo: action.payload,
      };
    case types.LOGIN_FAILURE:
      return {
        ...state,
        errorMessage: action.payload,
        loading: false
      };
    case types.USER_UPDATE_REQUEST:
      return {
        ...state,
        loading: true,
        updateUser: false
      };
    case types.USER_UPDATE_SUCCESS:
      return {
        ...state,
        loading: false,
        updateUser: true
      };
    case types.USER_UPDATE_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case types.REGISTER_ON_CHECKOUT_REQUEST:
      return {
        ...state,
        checkOutRegister: false,
        loadingCheckout: true
      };
    case types.REGISTER_ON_CHECKOUT_SUCCESS:
      return {
        ...state,
        checkOutRegister: true,
        loadingCheckout: true
      };
    case types.REGISTER_ON_CHECKOUT_FAIL:
      return {
        ...state,
        checkOutRegister: false,
        loadingCheckout: false
      };
    case types.LOGOUT_REQUEST:
      return {
        ...state,
        logOutStatus: false,
      }
    case types.LOGOUT_SUCCESS:
      return {
        ...state,
        token: undefined,
        logOutStatus: true,
        loggedInStatus: false,
      }
    case types.REMOVE_ERROR:
      return {
        ...state,
        errorMessage: ""
      }
    case types.CHANGE_PASSWORD_REQUEST:
      return {
        ...state,
        loading: true,
        changePassword: false
      }
    case types.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        changePassword: true
      }
    case types.CHANGE_PASSWORD_FAILURE:
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}

export default userReducer
