import { Product } from '../products/types'
import Option from '../../interfaces/Option'
import { Store } from '../products/types'

const ADD_PRODUCT_TO_CART: string = 'ADD_TO_CART'
const REMOVE_PRODUCT_FROM_CART: string = 'REMOVE_FROM_CART'
const UPDATE_PRODUCT_IN_CART: string = 'UPDATE_PRODUCT_IN_CART'
const GRAND_TOTAL_SAVE: string = 'GRAND_TOTAL_SAVE'
const ORDER_DATA_REQUEST: string = 'ORDER_DATA_REQUEST'
const ORDER_DATA_SUCCESS: string = 'ORDER_DATA_SUCCESS'
const ORDER_DATA_FAILURE: string = 'ORDER_DATA_FAILURE'
const START_NEW_ORDER: string = 'START_NEW_ORDER'

export const types = {
  ADD_PRODUCT_TO_CART,
  REMOVE_PRODUCT_FROM_CART,
  UPDATE_PRODUCT_IN_CART,
  GRAND_TOTAL_SAVE,
  ORDER_DATA_REQUEST,
  ORDER_DATA_SUCCESS,
  ORDER_DATA_FAILURE,
  START_NEW_ORDER
}

export interface CartItem {
  id?: number
  product: Product,
  quantity: number,
  size?: any,
  ingredients?: Option[],
  extraIngredients?: Option[],
  sizePrice: any
  totalPrice: number,
  grandTotal?: number,
  //store:Store,
}

export interface HalfHalfCartItem {
  id?: number
  products: Product[],
  quantity: number[],
  size?: any[],
  ingredients?: (Option[])[],
  extraIngredients?: (Option[])[],
  sizePrices: any[]
  totalPrices: number[],
  grandTotal?: number,
}

export interface Cart {
  cart: CartItem[]
  grandTotal: number,
  loading: boolean,
  orderResponse: string,
  isOrderSubmit: boolean
}
export interface total {
  grandTotal: number
  items?: any
  cartIs?: any
}

export interface orderData {
  storeId: any,
  email: string,
  totalAmount: number,
  orderStatus: string,
  items: any
}

export interface AddProductToCart {
  type: typeof ADD_PRODUCT_TO_CART
  payload: CartItem
}

export interface UpdateProductInCart {
  type: typeof UPDATE_PRODUCT_IN_CART
  payload: CartItem
}

export interface RemoveProductFromCart {
  type: typeof REMOVE_PRODUCT_FROM_CART
  payload: number
}

export interface GrandTotal {
  type: typeof GRAND_TOTAL_SAVE
  payload: total
}

export interface OrderData {
  type: typeof ORDER_DATA_REQUEST
  payload: orderData
}

export interface StartOerder {
  type: typeof START_NEW_ORDER
}

export type CartActionTypes = AddProductToCart | RemoveProductFromCart | UpdateProductInCart | GrandTotal | StartOerder | OrderData
