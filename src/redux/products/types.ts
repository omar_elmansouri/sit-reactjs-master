const LOAD_CATEGORY: string = 'LOAD_CATEGORY'
const LOAD_CATEGORY_SUCCESS: string = 'LOAD_CATEGORY_SUCCESS'
const LOAD_CATEGORY_FAILURE: string = 'LOAD_CATEGORY_FAILURE'
const SEARCH_PRODUCTS: string = 'SEARCH_PRODUCTS'

export const types = {
  LOAD_CATEGORY,
  LOAD_CATEGORY_SUCCESS,
  LOAD_CATEGORY_FAILURE,
  SEARCH_PRODUCTS
}

export interface ItemOptionGroup {
  id: number
  name: string
  choice: string
  price: number
  optionName: string
  activeOptionName: string
}

export interface ItemOption {
  id: number
  name: string
  choice: string
  price: number
  itemOptionGroups: ItemOptionGroup[]
  item: object[]
}

export interface Product {
  id: number
  categoryId: number
  itemName: string
  price: number
  taxRateIfPickUp: number
  taxRateIfDineIn: number
  information: string
  ingredient: string
  image: string
  imageContentType: string
  itemOptions: ItemOption[]
  itemRatings?: any
  from: string
  loading?: boolean
  category?: any

}

export interface Store {
  id: number
  name: string
  location: string
  banner: string
  logo: string
  logoContentType: string
  numberOfTables: number
  availability: boolean
  apiKey: string
  payLater: boolean
  askForService: boolean
  enableSms: boolean
  slug: string
  alias: string
  webhookUrl: string
  redirectUrl: string
  orders?: any
  users?: any
  qtables?: any
}

export interface Category {
  id?: number
  name: string
  image?: string
  imageContentType?: string
  items?: Product[]
  store?: Store[]
  scrollToCategory?: any
  current?: any
  loading?: boolean

}

export interface SearchInterface {
  search: string
}
export interface LoadCategory {
  type: typeof LOAD_CATEGORY
  payload: any
}

export interface SearchProducts {
  type: typeof SEARCH_PRODUCTS
  payload: Store
}


export type ProductActionTypes = LoadCategory | SearchProducts
