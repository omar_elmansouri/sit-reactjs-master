
import { types,LoadCategory } from './types'

export const loadCategory = (): LoadCategory => {
  return {
    type: types.LOAD_CATEGORY,
    payload: ""
  }
}
