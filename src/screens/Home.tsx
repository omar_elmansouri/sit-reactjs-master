import React from 'react'
import Page from '../components/Page'
import Header from '../components/Header'
import StickyHeader from '../components/StickyHeader'
import OrderList from '../components/OrderList'
import ProductsList from '../components/ProductsList'
import CategoriesList from '../components/CategoriesList'
import ProductFiltersData from '../mock/ProductFIltersData'
import CategoryFilter from '../interfaces/CategoryFilter'
import Option from '../interfaces/Option'
import LoginModal from '../components/modals/LoginModal'
import RegisterModal from '../components/modals/RegisterModal'
import ForgotPasswordModal from '../components/modals/ForgotPasswordModal'
import { connect } from 'react-redux'
import ProductDetailModal from '../components/modals/ProductDetailModal'
import HalfHalfModal from '../components/modals/HalfHalfModal'
import { ProductIngredientsOptions, ProductExtraIngredientsOptions } from '../mock/ProductOptionsData'
import SizeOptionsData from '../mock/SizeOptionsData'
import { loginUser } from '../redux/user/actions'
import { User } from '../redux/user/types'
import { loadCategory } from '../redux/products/actions'
import CheckoutModal from '../components/modals/CheckoutModal'
import OrderSubmittedModal from '../components/modals/OrderSubmittedModal'
import { Category, Product } from '../redux/products/types'
import { CartItem } from '../redux/cart/types'
import { addProductToCart, startNewOrderAction } from '../redux/cart/actions'
import ItemAddedModal from '../components/modals/ItemAddedModal'
import CategoriesListMobile from '../components/mobile/CategoriesListMobile'
import ProductsListMobile from '../components/mobile/ProductsListMobile'
import HeaderMobile from '../components/mobile/HeaderMobile'
import LeftSideBar from '../components/mobile/LeftSideBar'
import RightSideBar from '../components/mobile/RightSideBar'
import CartIcon from '../assets/icons/cart.png'
import RightIcon from '../assets/icons/right-icon-white.png'
import OrderModalMobile from '../components/mobile/OrderModalMobile'
import CustomerDetailModel from '../components/modals/CustomerDetailModel'
import PaymentCheckoutModel from '../components/modals/PaymentCheckoutModel'
import Loader from 'react-loader-spinner'
import HotSaleModal from '../components/modals/HotSaleModal'
import PickupDeliveryModal from '../components/modals/PickupDeliveryModal'
import OnlineHoursModal from '../components/modals/OnlineHoursModal'
import StoreLocationModal from '../components/modals/StoreLocationModal'
import OopsModal from '../components/modals/OopsModal'
import AlsoLikeModal from '../components/modals/AlsoLikeModal'

interface State {
  filters: CategoryFilter[]
  activeFilters: CategoryFilter[]
  selectedCategory?: Category
  options: Option[]
  extraOptions: Option[]
  selectedOptions: Option[]
  selectedExtraOptions: Option[]
  sizes: Option[]
  selectedSize: Option
  cartSlideVisible: boolean
  categorySlideVisible: boolean
  loginModalVisible: boolean
  oopsModalVisible: boolean
  errorMessage: string
  registerModalVisible: boolean
  forgotModalVisible: boolean
  pickupDeliveryModalVisible: boolean
  onlineHoursModalVisible: boolean
  storeLocationModalVisible: boolean
  hotSaleModalVisible: boolean
  productDetailModalVisible: boolean
  halfHalfModalVisible: boolean
  alsoLikeModalVisible: boolean
  checkoutModalVisible: boolean
  orderSubmittedModalVisible: boolean
  itemAddedModalVisible: boolean
  selectedProduct?: Product
  mobileLeftSidebar: boolean
  mobileRightSidebar: boolean
  mobileOrderModalVisible: boolean
  customerDetailWithOrderVisible: boolean
  isTop: boolean
  paymentModelVisible: boolean
}

interface Props {
  user?: any
  products?: any
  cart?: any
  auth?: any
  load?: any

  loginUser?(user: User): void

  loadCategory?(): void

  addProductToCart?(cartItem: CartItem): void
  newOrder?(): void
}

class Home extends React.Component<Props, State> {
  state = {
    filters: [...ProductFiltersData] as CategoryFilter[],
    activeFilters: [] as CategoryFilter[],
    selectedCategory: undefined,
    options: [...ProductIngredientsOptions] as Option[],
    extraOptions: [...ProductExtraIngredientsOptions] as Option[],
    selectedOptions: [...ProductIngredientsOptions] as Option[],
    selectedExtraOptions: [] as Option[],
    sizes: SizeOptionsData as Option[],
    selectedSize: { ...SizeOptionsData[0] } as Option,
    cartSlideVisible: false,
    categorySlideVisible: false,
    loginModalVisible: false,
    oopsModalVisible: false,
    errorMessage: '',
    registerModalVisible: false,
    forgotModalVisible: false,
    pickupDeliveryModalVisible: true,
    onlineHoursModalVisible: false,
    storeLocationModalVisible: false,
    hotSaleModalVisible: false,
    productDetailModalVisible: false,
    halfHalfModalVisible: false,
    alsoLikeModalVisible: false,
    checkoutModalVisible: false,
    paymentModelVisible: false,
    orderSubmittedModalVisible: false,
    itemAddedModalVisible: false,
    selectedProduct: undefined,
    mobileLeftSidebar: false,
    mobileRightSidebar: false,
    mobileOrderModalVisible: false,
    customerDetailWithOrderVisible: false,
    isTop: false,
  }

  componentDidMount = (): void => {
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY
      if (isTop >= 150) {
        this.onScroll(isTop)
      } else {
        this.setState({ isTop: false })
      }
    })
    if (this.props.products.length === 0)

      this.props.loadCategory()
    window.addEventListener('resize', this.closeSlides)
  }

  onScroll(isTop) {
    this.setState({ isTop: true })
  }

  componentWillReceiveProps(props: any) {
    if (props.auth.errorMessage) {
      this.onError(props.auth.errorMessage)
    }
    if (props.auth.loggedInStatus && this.props.auth.loggedInStatus !== props.auth.loggedInStatus) {
      this.setState({ loginModalVisible: false, registerModalVisible: false })
    }
    if (props.auth.checkOutRegister && this.props.auth.checkOutRegister !== props.auth.checkOutRegister) {
      this.setState({ customerDetailWithOrderVisible: false, loginModalVisible: true })
    }
  }

  componentWillUnmount = (): void => {
    window.removeEventListener('resize', this.closeSlides)
  }

  closeSlides = (e = {}): void => {
    this.setState({
      cartSlideVisible: false,
      categorySlideVisible: false,
    })
  }

  onRegisterModalToggle = (): void => {
    this.setState({ registerModalVisible: !this.state.registerModalVisible, loginModalVisible: false })
  }

  onLoginModalToggle = (): void => {
    this.setState({ loginModalVisible: !this.state.loginModalVisible, registerModalVisible: false })
  }
  onError = (errorMessage): void => {
    {errorMessage && this.setState({errorMessage})}
    this.onOopsModalToggle();
    this.onLoginModalToggle();
  }
  onOopsModalToggle = (): void =>{
    this.setState({oopsModalVisible:!this.state.oopsModalVisible})
  }
  onLoginPress = (): void => {
    // this.onLoginModalToggle()
    if (this.props.loginUser) {
      const user = {
        username: 'Demo',
        password: '',
      }
      this.props.loginUser(user)
    }
  }
  onForgotModalToggle = (): void => {
    this.setState({ forgotModalVisible: !this.state.forgotModalVisible, loginModalVisible: false })
  }
  onHotSaleModalToggle = (): void => {
    this.setState({ hotSaleModalVisible: !this.state.hotSaleModalVisible})
  }
  onPickupDeliveryModalToggle = (): void => {
    this.setState({
      pickupDeliveryModalVisible: !this.state.pickupDeliveryModalVisible,
      hotSaleModalVisible: true
    })
  }
  onOnlineHoursModalToggle = (): void => {
    this.setState({ onlineHoursModalVisible: !this.state.onlineHoursModalVisible})
  }
  onStoreLocationModalToggle = (): void => {
    this.setState({ storeLocationModalVisible: !this.state.storeLocationModalVisible})
  }

  onCategoryPress = (category: Category): void => {
    this.setState({ selectedCategory: category })
  }

  onMobileProductsListBack = (): void => {
    this.setState({ selectedCategory: undefined })
  }

  onIngredientToggle = (option: Option): void => {
    this.setState({ selectedOptions: this.toggleOptionInOptionsArray(option, this.state.selectedOptions) })
  }

  onExtraIngredientToggle = (option: Option): void => {
    this.setState({ selectedExtraOptions: this.toggleOptionInOptionsArray(option, this.state.selectedExtraOptions) })
  }

  toggleOptionInOptionsArray = (option: Option, options: Option[]): Option[] => {
    const index: number = options.indexOf(option)
    if (index > -1) {
      options.splice(index, 1)
    } else {
      options.push(option)
    }
    return options
  }

  onSizePress = (size: Option): void => {
    this.setState({ selectedSize: size })
  }

  onProductClick = (product: Product): void => {
    this.setState({ productDetailModalVisible: true, selectedProduct: product })
  }
  onHalfHalfClick = (): void => {
    this.setState({ halfHalfModalVisible: true })
  }

  itemAddedToggle = (): void => {
    this.setState({ itemAddedModalVisible: true }, () => {
      setTimeout(() => {
        this.setState({ itemAddedModalVisible: false })
      }, 1000)
    })
  }

  closeItemAddedToggle = (): void => {
    this.setState({ itemAddedModalVisible: false })
  }

  onAlsoLikeClick = (): void => {
    this.setState({ alsoLikeModalVisible: true })
  }
  closeAlsoLikeToggle = (): void => {
    this.setState({ alsoLikeModalVisible: false })
  }
  onCheckoutClick = (): void => {
    this.setState({ checkoutModalVisible: true })
  }
  onCustomerLogin = (): void => {
    this.setState({ customerDetailWithOrderVisible: true, mobileOrderModalVisible: false })
  }

  onSubmitClick = (): void => {
    this.closeCheckoutToggle()
    this.props.newOrder();
    this.setState({ orderSubmittedModalVisible: true })
  }
  onSubmitPayment = (): void => {
    this.setState({
      paymentModelVisible: true,
      checkoutModalVisible: false,
    })
  }

  onAddItem = (cartItem: CartItem): void => {
    this.closeProductDetailToggle()
    this.itemAddedToggle()
    if (cartItem.quantity > 0 && this.props.addProductToCart) {
      this.props.addProductToCart(cartItem)
    }
  }
  onCategoryFilterToggle = (categoryFilter: CategoryFilter): void => {
    const { activeFilters } = this.state
    const index: number = activeFilters.indexOf(categoryFilter)
    if (index > -1) {
      activeFilters.splice(index, 1)
    } else {
      activeFilters.push(categoryFilter)
    }
    this.setState({ activeFilters })
  }

  onCartSlideToggle = (): void => {
    this.setState({ cartSlideVisible: !this.state.cartSlideVisible })
  }

  onCategorySlideToggle = (): void => {
    this.setState({ categorySlideVisible: !this.state.categorySlideVisible })
  }

  closeProductDetailToggle = (): void => {
    this.setState({ productDetailModalVisible: false, selectedProduct: undefined })
  }
  closeHalfHalfToggle = (): void => {
    this.setState({ halfHalfModalVisible: false })
  }

  closeCheckoutToggle = (): void => {
    this.setState({ checkoutModalVisible: false })
  }
  closePaymentToggle = (): void => {
    this.setState({ paymentModelVisible: false })
  }

  closeSubmittedToggle = (): void => {
    this.setState({ orderSubmittedModalVisible: false })
  }

  closeCustomerLoginToggle = (): void => {
    this.setState({ customerDetailWithOrderVisible: false })
  }

  renderHeaders = () => {
    const navbarSticky = this.state.isTop
    return (
      <>
        <div className={navbarSticky ? 'headerOpacityVisible' : 'headerStickyNotVisible'}><StickyHeader
          onRegister={this.onRegisterModalToggle}
          onLogin={this.onLoginModalToggle}
          onCartToggle={this.onCartSlideToggle}
          onCategoryToggle={this.onCategorySlideToggle}
          onAccount={() => null}
        /></div>
        <div className={navbarSticky ? 'headerNotVisible' : 'headerOpacityVisible'}><Header
          onRegister={this.onRegisterModalToggle}
          onLogin={this.onLoginModalToggle}
          onCartToggle={this.onCartSlideToggle}
          onCategoryToggle={this.onCategorySlideToggle}
          onAccount={() => null}
        /></div>

        <HeaderMobile onLeftPress={() => this.setState({
          mobileLeftSidebar: !this.state.mobileLeftSidebar,
          mobileRightSidebar: false,
        })}
                      onRightPress={() => this.setState({
                        mobileLeftSidebar: false,
                        mobileRightSidebar: !this.state.mobileRightSidebar,
                      })}
                      leftBar={this.state.mobileLeftSidebar}
        />
      </>
    )
  }

  renderMobileContent = () => {
    const { cart } = this.props
    let grandTotal = cart.cart.map((cartItem: CartItem) => {
      return cartItem.quantity * (cartItem.product.price + cartItem.totalPrice)
    })
    let GrandTotal = 0
    for (let i = 0; i < grandTotal.length; i++) {
      GrandTotal += grandTotal[i]
    }
    return (
      <div className="mobile">
        {this.state.selectedCategory ?
          <ProductsListMobile
            onClick={this.onProductClick}
            onBack={this.onMobileProductsListBack}
            category={this.state.selectedCategory}/> :
          <CategoriesListMobile onCategoryPress={this.onCategoryPress}/>}
        <LeftSideBar onClose={() => this.setState({ mobileLeftSidebar: false })}
                     visible={this.state.mobileLeftSidebar}
                     onRegister={this.onRegisterModalToggle}
                     onLogin={this.onLoginModalToggle}
        />
        <RightSideBar onClose={() => this.setState({ mobileRightSidebar: false })}
                      visible={this.state.mobileRightSidebar}
                      categoryFiltersData={this.state.filters}
                      onToggleCategoryFilter={this.onCategoryFilterToggle}
                      selectedCategoryFilters={[...this.state.activeFilters]}
        />
        {this.props.cart && this.props.cart.cart.length > 0 && (
          <div className={'order-bar'} onClick={() => this.setState({ mobileOrderModalVisible: true })}>
            <div className={'cart-icon'}>
              <img src={CartIcon} alt={'Cart Icon'} className={'icon'}/>
            </div>
            <h2>${GrandTotal}</h2>
            <div className={'checkout'}>
              <h2>CHECKOUT</h2>
              <img src={RightIcon} alt={'next icon'}/>
            </div>
          </div>
        )}

        <OrderModalMobile
          onCustomerLogin={this.onCustomerLogin}
          onAlsoLikeClick={this.onAlsoLikeClick}
          onReceiptClick={this.onProductClick}
          onClose={() => this.setState({ mobileOrderModalVisible: false })}
          visible={this.state.mobileOrderModalVisible}/>
      </div>
    )
  }
  renderNormalSizeContent = () => {
    const sticky = this.state.isTop
    return (
      <>
        {sticky ? <div style={{ padding: '160px 15px 0px 15px' }} className={'normal'}>
            <CategoriesList onCategoryPress={this.onCategoryPress} slideVisible={this.state.categorySlideVisible}
                            onSlideToggle={this.onCategorySlideToggle}
            />
            <ProductsList
              categoryFiltersData={this.state.filters}
              onToggleCategoryFilter={this.onCategoryFilterToggle}
              selectedCategoryFilters={[...this.state.activeFilters]}
              onClick={this.onProductClick}
              onHalfClick={this.onHalfHalfClick}
            />
            <OrderList slideVisible={this.state.cartSlideVisible}
                       onSlideToggle={this.onCartSlideToggle}
                       onAlsoLikeClick={this.onAlsoLikeClick}
                       onReceiptClick={this.onProductClick}
            />
          </div> :
          <div className={'normal'}>
            <CategoriesList onCategoryPress={this.onCategoryPress} slideVisible={this.state.categorySlideVisible}
                            onSlideToggle={this.onCategorySlideToggle}
            />
            <ProductsList
              categoryFiltersData={this.state.filters}
              onToggleCategoryFilter={this.onCategoryFilterToggle}
              selectedCategoryFilters={[...this.state.activeFilters]}
              onClick={this.onProductClick}
              onHalfClick={this.onHalfHalfClick}
            />
            <OrderList slideVisible={this.state.cartSlideVisible}
                       onSlideToggle={this.onCartSlideToggle}
                       onAlsoLikeClick={this.onAlsoLikeClick}
                       onReceiptClick={this.onProductClick}
            />
          </div>
        }
      </>
    )
  }

  renderModals = () => {
    return (
      <>
        <LoginModal onClose={this.onLoginModalToggle} visible={this.state.loginModalVisible} onLogin={this.onLoginPress}
                    onDontHaveAccount={this.onRegisterModalToggle} onError={this.onError}
        />
        <OopsModal onClose={this.onOopsModalToggle} visible={this.state.oopsModalVisible} errorMessage={this.state.errorMessage}/>
        <RegisterModal onClose={this.onRegisterModalToggle} visible={this.state.registerModalVisible}
                       onAlreadyHaveAnAccount={this.onLoginModalToggle}
        />
        <ForgotPasswordModal onClose={this.onForgotModalToggle} visible={this.state.forgotModalVisible}
        />
        <HotSaleModal onClose={this.onHotSaleModalToggle} visible={this.state.hotSaleModalVisible}
        />
        <PickupDeliveryModal onOnlineHoursClick={this.onOnlineHoursModalToggle} onViewMapClick={this.onStoreLocationModalToggle} onClose={this.onPickupDeliveryModalToggle} visible={this.state.pickupDeliveryModalVisible}
        />
        <OnlineHoursModal onClose={this.onOnlineHoursModalToggle} visible={this.state.onlineHoursModalVisible}
        />
        <StoreLocationModal onClose={this.onStoreLocationModalToggle} visible={this.state.storeLocationModalVisible}
        />
        <ProductDetailModal
          product={this.state.selectedProduct}
          sizesData={this.state.sizes}
          // ingredients={this.state.options}
          selectedOptions={this.state.selectedOptions}
          // extraIngredients={this.state.extraOptions}
          onOptionPress={this.onIngredientToggle}
          onExtraIngredientPress={this.onExtraIngredientToggle}
          onSizePress={this.onSizePress}
          selectedExtraOptions={this.state.selectedExtraOptions}
          selectedSize={this.state.selectedSize}
          onAddItem={this.onAddItem}
          onClose={this.closeProductDetailToggle}
          visible={this.state.productDetailModalVisible}
        />
        <HalfHalfModal
          sizesData={this.state.sizes}
          // ingredients={this.state.options}
          selectedOptions={this.state.selectedOptions}
          // extraIngredients={this.state.extraOptions}
          onOptionPress={this.onIngredientToggle}
          onExtraIngredientPress={this.onExtraIngredientToggle}
          onSizePress={this.onSizePress}
          selectedExtraOptions={this.state.selectedExtraOptions}
          selectedSize={this.state.selectedSize}
          onAddItem={this.onAddItem}
          onClose={this.closeHalfHalfToggle}
          visible={this.state.halfHalfModalVisible}
        />
        <ItemAddedModal
          visible={this.state.itemAddedModalVisible}
          close={this.closeItemAddedToggle}
        />
        <AlsoLikeModal
          onCustomerLogin={this.onCustomerLogin}
          onCheckoutClick={this.onCheckoutClick}
          onProductClick={this.onProductClick}
          onClose={this.closeAlsoLikeToggle} visible={this.state.alsoLikeModalVisible}
        />
        <CheckoutModal
          onSubmitClick={this.onSubmitClick}
          paymentModel={this.onSubmitPayment}
          onClose={this.closeCheckoutToggle} visible={this.state.checkoutModalVisible}
        />
        <PaymentCheckoutModel
          onClose={this.closePaymentToggle} visible={this.state.paymentModelVisible}
        />
        <CustomerDetailModel
          visible={this.state.customerDetailWithOrderVisible}
          onClose={this.closeCustomerLoginToggle}
        />
        <OrderSubmittedModal
          onClose={this.closeSubmittedToggle} visible={this.state.orderSubmittedModalVisible}
        />
      </>
    )
  }

  render(): React.ReactNode {
    let loading = this.props.load;
    return (
      <div>
        {loading ? <div className={'fullScreenOverlay center'}>
          <Loader
            type="Oval"
            color="#A6A6AB"
            height={50}
            width={70}
            timeout={700000}
          />
        </div> : <Page user={this.props.user}>
          {this.renderHeaders()}
          <section id={'main'}>
            {this.renderMobileContent()}
            {this.renderNormalSizeContent()}
          </section>
          {this.renderModals()}
        </Page>}

      </div>
    )
  }
}

const mapStateToProps = (state: any): any => {
  return {
    cart: state.cart,
    user: state.user,
    products: state.products.category,
    auth: state.auth,
    load: state.products.loading,
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    loadCategory: () => dispatch(loadCategory()),
    addProductToCart: (cartItem: CartItem) => dispatch(addProductToCart(cartItem)),
    loginUser: (user: User) => dispatch(loginUser(user)),
    newOrder: () => dispatch(startNewOrderAction()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
