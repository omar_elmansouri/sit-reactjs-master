import React from 'react'
import HalfHalfImage from '../assets/images/pizza.jpg'
import BreadIcon from '../assets/icons/bread.png'
import CloseIcon from '../assets/icons/cross-white.png'
import SearchIcon from '../assets/icons/search.png'
import RightIcon from '../assets/icons/right-icon.png'
import CategoryFilter from '../interfaces/CategoryFilter'
import { connect } from 'react-redux'
import { Product } from '../redux/products/types'
import { Element } from 'react-scroll'
import { withTranslation, WithTranslation } from 'react-i18next';

import { Link} from 'react-scroll'
interface Props extends WithTranslation  {
  categoryFiltersData: CategoryFilter[]
  selectedCategoryFilters: CategoryFilter[]

  onToggleCategoryFilter(category: CategoryFilter): void

  onClick(product: Product): void

  onHalfClick(): void

  data?: any
}

interface State {
  search: string
  data: any
  filterData: any
  visible: boolean
  
}

class ProductsList extends React.Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      search: '',
      data: [],
      filterData: [],
      visible: false,
    }
  }

  componentWillReceiveProps(nextProps: Readonly<Props>, nextContext: any) {
    this.setState({
      filterData: nextProps.data,
      data: nextProps.data,
    })
  }

  onSearchPress = () => {
    this.setState({ visible: !this.state.visible, search: '' })
  }

  onSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [e.target.name]: e.target.value,
    } as Pick<State, any>)
  }

  render(): React.ReactNode {
    
    const {
      selectedCategoryFilters,
      onToggleCategoryFilter,
      onClick,
      onHalfClick,
      data,
    } = this.props
    const { search } = this.state
    const{t}=this.props;
    let categories = this.props.data || []
    return (
      <>
        <div className={'products'}>
        <div className={'filter-bar'}>
            <div>
            <div>

             <div className='dropfilter'>

             
            
                 
              </div>    
                   
             
             <ul >
              {categories.map((category, index) => (
         <>
              <li key={index} onClick={() =>onToggleCategoryFilter(category)}>
            <Link
 
             className={t(category.name)}
              to={category.id.toString()}
              spy={true}
              smooth={true}
               duration={300}
               offset={-200}
                 delay={100}
              >
        <button  className='categorybtn'>
        {t(category.name)}
        </button>
  
        </Link>
         </li>

       </>
      ))}
      </ul>
    


  

</div>
              <div className={'filters'}>
                {data.map((category, index) => {
                  const isActive = selectedCategoryFilters.includes(category)
                  return (
                    <div
                      key={index}
                      className={`circle-icon filter-item ${
                        isActive ? 'active' : ''
                      }`}
                      onClick={() => {
                        !isActive && onToggleCategoryFilter(category)
                      }}
                    >
                      <div className={'icon'}>
                        <img src={BreadIcon} alt={'Bread icon'}/>
                      </div>
                      <p className={'label'}>{t(category.name)}</p>
                      <img
                        src={CloseIcon}
                        alt="Close filter"
                        className={'close-icon'}
                        onClick={() => {
                          onToggleCategoryFilter((category))
                        }}
                      />
                    </div>
                  )
                })}
              </div>
            </div>
            <div className={`search-bar ${!this.state.visible ? 'no-padding' : ''}`}>
              {this.state.visible &&
              <form autoComplete={'off'} onSubmit={(e) => e.preventDefault()}>
                <input onChange={this.onSearch} autoComplete={'off'} name="search" value={search} type="search"
                       placeholder={t('Search for')}/>
              </form>
              }
              <div className={'circle-icon'}>
                <div className={'icon no-border'}>
                  <img src={SearchIcon} alt={'Search icon'} onClick={this.onSearchPress}/>
                </div>
              </div>
            </div>
          </div>
          {
            data.filter(item => selectedCategoryFilters.length === 0 || selectedCategoryFilters.includes(item)).map((category, index) => {
              return (
                <Element
                  name={category.id.toString()}
                  className={category.id.toString()}
                  id={category.id.toString()}
                  key={'display' +category.id}
                >
                  <div className={'category-products'}>
                    <h3 className={'title'}>{t(category.name)}</h3>
                    <p className={'sub'}>
                    {t('All')} { t(category.name)}</p>
                    
                    <div className={'items'}>
                      {category.items.filter(product => product && product.itemName && product.itemName.toLowerCase().indexOf(search.toLowerCase()) > -1).map((product, index) => (
                        product.itemName === 'Pizza of 2 sides' ? (
                            <div className={'product-horizontal'} onClick={() => onHalfClick()}>
                              <div className={'product-item'}>
                                <img
                                  src={`data:${product.imageContentType};base64,${product.image}`}
                                  alt=""
                                  sizes={'700vm'}
                                  className={'product-image'}
                                />
                                <div className={'product-info'}>
                                  <h4 className={'name'}>{t(product.itemName)}</h4>
                                  <p className={'description'}></p>
                                  <div>
                                    <img src={RightIcon} alt="Right icon"/>
                                  </div>
                                </div>
                              </div>
                            </div>)
                          : (<div className={'product-horizontal'} key={index} onClick={() => onClick(product)}>
                              <div className={'product-item'}>
                                <img
                                  src={`data:${product.imageContentType};base64,${product.image}`}
                                  alt=""
                                  sizes={'700vm'}
                                  className={'product-image'}
                                />
                                <div className={'product-info'}>
                                  <h4 className={'name'}>{t(product.itemName)}</h4>
                                  <p className={'description'}>
                                    {product.ingredient}
                                  </p>
                                  <div>
                                    <p className='price'>from ${product.price.toFixed(2)}</p>
                                    <img src={RightIcon} alt="Right icon"/>
                                  </div>
                                </div>
                              </div>
                            </div>
                          )))}
                      {[...Array(3 - category.items.length % 3)].map(dummy => (
                        <div className={'product-container dummy'}/>
                      ))}
                    </div>
                  </div>
                </Element>
              )
            })
          }
        </div>
      </>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.products.category,
    load: state.products.loading,
  }
}
export default withTranslation()(connect(mapStateToProps, null)(ProductsList))

