import React, { Component } from 'react'
import { WithTranslation } from 'react-i18next'
import CloseTag from '../CloseTag'

interface Props {
  onClose?(): void

  visible: boolean
}

class StoreLocationModal extends React.Component<Props> {
  render(): React.ReactNode {
    const {
      onClose,
      visible,
    } = this.props
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body sm'}>
          <div className={'white-header ph'}>
            <CloseTag onClose={onClose}/>
            <h3>Store Location</h3>
          </div>
          <div className={'content'}>
            <iframe
              src={'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3323.63535495568!2d-7.640030085411354!3d33.588815749457375!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xda7d2ec07cb58cb%3A0x1d0fd64bec81a689!2sAtlassima!5e0!3m2!1sfr!2sma!4v1615905206152!5m2!1sfr!2sma'}
              width={320} frameBorder={0} style={{ border: 0 }} allowFullScreen={false}
              aria-hidden={false}
              tabIndex={0}/>
          </div>
        </div>
      </div>
    )
  }
}

export default StoreLocationModal
