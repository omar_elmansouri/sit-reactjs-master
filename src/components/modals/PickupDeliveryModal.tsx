import React from 'react'
import HorizontalLine from '../HorizontalLine'
import TickIcon from '../../assets/icons/tick-white.png'
import CashIcon from '../../assets/icons/cash.png'
import { withTranslation, WithTranslation } from 'react-i18next'
import { FaCarSide, FaMoneyBill, FaRegCreditCard } from 'react-icons/fa'
import VerticalLine from '../VerticalLine'
import Logo from '../../assets/images/logo.png';

interface Props {
  onClose?(): void
  onOnlineHoursClick?():void
  onViewMapClick?():void
  visible: boolean
}

class PickupDeliveryModal extends React.Component<Props> {

  onDeliveryClick = () =>{
    this.props.onClose();
  }

  render() {
    const {
      onClose,
      onOnlineHoursClick,
      onViewMapClick,
      visible,

    } = this.props
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body  md'}>
          <div style={{paddingTop: 20, paddingBottom: 30, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center'}}>
            <img src={Logo} alt={'Logo'} style={{width: 120}}/>
          </div>
          <div className={'white-header ph'}>
            <h3>Atlassimagroup</h3>
            <p className={'sub grey'}>A24/ 24 Lexington Drive</p>
            <div className={'row'}>
              <div className={'grey-button'} onClick={onViewMapClick}>
                <p>VIEW MAP</p>
              </div>
              <div className={'grey-button'} onClick={onOnlineHoursClick}>
                <p>ONLINE HOURS</p>
              </div>
            </div>
            <p className={'skip-button'} onClick={onClose}>Skip to menu</p>
            <HorizontalLine/>
          </div>
          <div className={'content row-space-between'}>
            <div className={'option'} onClick={onClose}>
              <div className={'delivery'}>
                <p className={'open'}>Open</p>
                <FaMoneyBill className={'delivery-icon'}/>
              </div>
              <h4>PICKUP</h4>
              <p className={'time'}>15 mins</p>
            </div>
            <VerticalLine/>
            <div className={'option'} onClick={()=>null}>
              <div className={'delivery'}>
                <p className={'open'}>Open</p>
                <FaCarSide className={'delivery-icon'}/>
              </div>
              <h4>DELIVERY</h4>
              <p className={'time'}>45 mins</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default PickupDeliveryModal
