import React from 'react'
import CloseTag from '../CloseTag'
import HorizontalLine from '../HorizontalLine'
import Option from '../../interfaces/Option'
import PlusIcon from '../../assets/icons/plus.png'
import MinusIcon from '../../assets/icons/minus.png'
import VerticalLine from '../VerticalLine'
import { CartItem } from '../../redux/cart/types'
import { Product } from '../../redux/products/types'
import SizeOptionCheckbox from '../SizeOptionCheckbox'

interface Props {
  product?: Product,
  sizesData: Option[]
  selectedOptions: Option[]
  selectedExtraOptions: Option[]
  selectedSize: Option
  onOptionPress(option: Option): void
  onExtraIngredientPress(option: Option): void
  onSizePress(size: Option): void
  onAddItem?(cartItem: CartItem): void
  onClose?(): void
  visible: boolean
}
interface arrayObject {
  name: string;
  price: number;
}

interface State {
  quantity: number;
  selected: any[];
}

class ProductDetailModal extends React.Component<Props, State> {
  state: Readonly<State> = {
    quantity: 1,
    selected: [],
  }

  componentWillUnmount() {
    this.setState({quantity:1})
  }

  increaseQuantity = () => {
    this.setState({ quantity: this.state.quantity+1 })
  }
  decreaseQuantity = () => {
    if (this.state.quantity > 1) {
      this.setState({ quantity: this.state.quantity-1 })
    }
  }
  onAddItem = () => {
    let totalPrice = 0
    for(let i=0; i<this.state.selected.length; i++){
      totalPrice += this.state.selected[i].price
    }
    if (this.props.onAddItem && this.props.product) {
      const size = this.state.quantity * ((this.props.product?.price || 0) + totalPrice)
      const cartItem: CartItem = {
        product: this.props.product,
        quantity: this.state.quantity,
        size: this.state.selected,
        sizePrice: size,
        totalPrice: totalPrice
      }
      this.props.onAddItem(cartItem)
    }
  }

  render(): React.ReactNode {
    const {
      onClose,
      visible,
      product,
    } = this.props
    const result = {}
    for (let i = 0; i < product?.itemOptions.length; i++) {
      let group = product?.itemOptions[i].itemOptionGroups[0]
      if (!result[group.optionName]) {
        result[group.optionName] = [product?.itemOptions[i]]
      } else {
        result[group.optionName].push(product?.itemOptions[i])
      }
    }
    const groupIndexing = {}
    const merged = []
    for (let i = 0; i < product?.itemOptions.length; i++) {
      const optionGroupName = product.itemOptions[i].itemOptionGroups[0].optionName
      const itemOptions = result[optionGroupName]
      if (groupIndexing[optionGroupName] === undefined) {
        groupIndexing[optionGroupName] = i
        merged.push({
          groupName: optionGroupName,
          itemOptions,
        })
      }
    }
    let totalPrice = 0
    for(let i=0; i<this.state.selected.length; i++){
      totalPrice += this.state.selected[i].price
    }
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body  md'}>
          <div className={'header ph'}>
            <CloseTag onClose={onClose}/>
            <h3 className={'title'}>{product?.itemName}</h3>
            <p className={'sub'}>{product?.information}</p>
          </div>
          {merged.map((key) => {
            return (
              <div className={'content'}>
                <h5 className={'group-header'}>{key.groupName}</h5>
                <HorizontalLine/>
                <div className={'product-options'}>
                  {key.itemOptions.map((size) => {
                      // const isActive = this.state.selected === size.itemOptionGroups[0].activeOptionName
                      let isSelected = false
                      for(let i = 0; i <= this.state.selected.length; i++){
                        if(this.state.selected[i] && this.state.selected[i].id === size.id){
                          isSelected = true;
                          break;
                        }
                      }
                      return (
                        <SizeOptionCheckbox
                          onToggle={() => {
                            let foundIndex = -1;
                            for(let i = 0; i <= this.state.selected.length; i++){
                              console.warn(this.state.selected[i])
                              if(this.state.selected[i] && this.state.selected[i].id === size.id){
                                foundIndex = i;
                                break;
                              }
                            }
                            let {selected} = this.state;
                            if(foundIndex > -1) {
                              selected.splice(foundIndex, 1)
                            } else {
                              selected.push(size)
                            }
                            this.setState({selected})
                          }}
                          option={size}
                          label={size.choice}
                          price={size.price}
                          active={isSelected}/>
                      )
                    },
                  )}
                </div>
              </div>
            )
          })}
          <div className={'footer'}>
            <div className={'left'}>
              <div className={'quantity'}>
                <img
                  src={MinusIcon}
                  alt="Plus Amount"
                  className={'plus-minus-icon'}
                  onClick={this.decreaseQuantity}
                />
                <h3>{this.state.quantity}</h3>
                <img
                  src={PlusIcon}
                  alt="Minus Amount"
                  className={'plus-minus-icon'}
                  onClick={this.increaseQuantity}
                />
              </div>
              <VerticalLine/>
              <div className={'price'}>
                <h3>${(this.state.quantity * ((product?.price || 0) + totalPrice)).toFixed(2)}</h3>
              </div>
            </div>
            <div className={'right'} onClick={this.onAddItem}>
              <h3>Add Item</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ProductDetailModal
