import React from 'react'
import CloseTag from '../CloseTag'
import HorizontalLine from '../HorizontalLine'
import Option from '../../interfaces/Option'
import PlusIcon from '../../assets/icons/plus.png'
import MinusIcon from '../../assets/icons/minus.png'
import VerticalLine from '../VerticalLine'
import { CartItem } from '../../redux/cart/types'
import { Product } from '../../redux/products/types'
import SizeOptionCheckbox from '../SizeOptionCheckbox'
import EmptyImage from '../../assets/images/halfhalf_empty.png'
import { connect } from 'react-redux'
import RightIcon from '../../assets/icons/right-icon.png'


interface Props {
  data?: any
  sizesData: Option[]
  selectedOptions: Option[]
  selectedExtraOptions: Option[]
  selectedSize: Option

  onOptionPress(option: Option): void

  onExtraIngredientPress(option: Option): void

  onSizePress(size: Option): void

  onAddItem?(cartItem: CartItem): void

  onClose?(): void

  visible: boolean
}

interface arrayObject {
  name: string;
  price: number;
}

interface State {
  quantity: number[];
  selected: any[];
  products?: any[],
  productIndex?: number,
  merged: any[],
}

class HalfHalfModal extends React.Component<Props, State> {
  state = {
    quantity: [1, 1],
    selected: [],
    products: [],
    productIndex: 0,
    merged: [],
  }
  increaseQuantity = () => {
    const quantity = this.state.quantity
    quantity[this.state.productIndex]++
    this.setState({ quantity })
  }
  decreaseQuantity = () => {
    const quantity = this.state.quantity
    if (this.state.quantity[this.state.productIndex] > 1) {
      quantity[this.state.productIndex]--
      this.setState({ quantity })
    }
  }

  onNext = () => {
    if (this.state.merged.length !== 0) {
      console.log({ merged: this.state.merged })
      this.setState({ productIndex: 1 })
    }
  }
  onBack = () => {
    this.setState({ productIndex: 0 })
  }

  onAddItem = () => {
    let totalPrice = 0
    for (let i = 0; i < this.state.selected.length; i++) {
      totalPrice += this.state.selected[i].price
    }
    if (this.props.onAddItem && this.state.products) {
      const size = this.state.quantity[0] * ((this.state.products[0]?.price || 0) + totalPrice)
      const cartItem: CartItem = {
        product: this.state.products[0],
        quantity: this.state.quantity[0],
        size: this.state.selected,
        sizePrice: size,
        totalPrice: totalPrice,
      }
      this.props.onAddItem(cartItem)
    }
  }
  onProductClick = (product: Product) => {
    const products = this.state.products
    products[this.state.productIndex] = product
    this.setState({ products })
    const groupIndexing = {}
    const result = {}
    const merged = this.state.merged
    merged[this.state.productIndex] = []
    for (let i = 0; i < product?.itemOptions.length; i++) {
      let group = product?.itemOptions[i].itemOptionGroups[0]
      if (!result[group.optionName]) {
        result[group.optionName] = [product?.itemOptions[i]]
      } else {
        result[group.optionName].push(product?.itemOptions[i])
      }
    }
    for (let i = 0; i < product?.itemOptions.length; i++) {
      const optionGroupName = product.itemOptions[i].itemOptionGroups[0].optionName
      const itemOptions = result[optionGroupName]
      if (groupIndexing[optionGroupName] === undefined) {
        groupIndexing[optionGroupName] = i
        merged[this.state.productIndex].push({
          groupName: optionGroupName,
          itemOptions,
        })
      }
    }
    this.setState({ merged })
  }

  render(): React.ReactNode {
    console.log(this.props.data)
    const {
      onClose,
      visible,
    } = this.props

    let totalPrice = 0
    for (let i = 0; i < this.state.selected.length; i++) {
      totalPrice += this.state.selected[i].price
    }
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body  md'}>
          <div className={'header ph'}>
            <CloseTag onClose={onClose}/>
            <h3 className={'title'}>Half Half</h3>
          </div>
          <div className={'content halfHalf'}>
            <div className={'left'}>
              {console.log({ data: this.props.data })}
              <h3>{this.state.productIndex + 1}. CHOOSE {this.state.productIndex === 0 ? 'FIRST' : 'SECOND'} HALF</h3>
              <div className={'list'}>
                {this.props.data.length !== 0 && this.props.data.find(item => item && item.name === 'PIZZA').items.map((product, index) => product && product.itemName != 'Pizza of 2 sides' && (
                  <div className={'product-horizontal'} key={index} onClick={() => this.onProductClick(product)}>
                    <div className={'product-item halfList'}>
                      <img
                        src={`data:${product.imageContentType};base64,${product.image}`}
                        alt=""
                        sizes={'700vm'}
                        className={'halfProduct-image'}
                      />
                      <div className={'product-info halfHalf-info'}>
                        <h4 className={'name'}>{product.itemName}</h4>
                        <p className={'description'}>
                          {product.ingredient}
                        </p>
                        <div className={'bottom'}>
                          <p>${product.price.toFixed(2)}</p>
                          <img src={RightIcon} alt="Right icon"/>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
            <div className={'right'}>
              <h3 className={'product-title'}>{this.state.products[this.state.productIndex]?.itemName}</h3>
              {this.state.merged[this.state.productIndex] ? this.state.merged[this.state.productIndex].map((key) => {
                  return (
                    <div>
                      <h5 className={'group-header'}>{key.groupName}</h5>
                      <HorizontalLine/>
                      <div className={'product-options'}>
                        {key.itemOptions.map((size) => {
                            // const isActive = this.state.selected === size.itemOptionGroups[0].activeOptionName
                            let isSelected = false
                            for (let i = 0; i <= this.state.selected.length; i++) {
                              if (this.state.selected[i] && this.state.selected[i].id === size.id) {
                                isSelected = true
                                break
                              }
                            }
                            return (
                              <SizeOptionCheckbox
                                onToggle={() => {
                                  let foundIndex = -1
                                  for (let i = 0; i <= this.state.selected.length; i++) {
                                    console.warn(this.state.selected[i])
                                    if (this.state.selected[i] && this.state.selected[i].id === size.id) {
                                      foundIndex = i
                                      break
                                    }
                                  }
                                  let { selected } = this.state
                                  if (foundIndex > -1) {
                                    selected.splice(foundIndex, 1)
                                  } else {
                                    selected.push(size)
                                  }
                                  this.setState({ selected })
                                }}
                                option={size}
                                label={size.choice}
                                price={size.price}
                                active={isSelected}/>
                            )
                          },
                        )}
                      </div>
                    </div>
                  )
                }) :
                <div className={'empty'}>
                  <img src={EmptyImage}/>
                  <p>Please choose your item on the left first.</p>
                </div>
              }
            </div>
          </div>
          <div className={'footer'}>
            {this.state.productIndex === 1 &&
            <div className={'left back'} onClick={this.onBack}>
              <h3>Back</h3>
            </div>
            }
            <div className={'left'}>
              <div className={'quantity'}>
                <img
                  src={MinusIcon}
                  alt="Plus Amount"
                  className={'plus-minus-icon'}
                  onClick={this.decreaseQuantity}
                />
                <h3>{this.state.quantity[this.state.productIndex]}</h3>
                <img
                  src={PlusIcon}
                  alt="Minus Amount"
                  className={'plus-minus-icon'}
                  onClick={this.increaseQuantity}
                />
              </div>
              <VerticalLine/>
              <div className={'price'}>
                <h3>${(this.state.quantity[this.state.productIndex] * ((this.state.products[this.state.productIndex]?.price || 0) + totalPrice)).toFixed(2)}</h3>
              </div>
            </div>
            {this.state.productIndex === 0 ?
              <div className={'right'} onClick={this.onNext}>
                <h3>Next</h3>
              </div> :
              <div className={'right'} onClick={this.onAddItem}>
                <h3>Add Item</h3>
              </div>}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.products.category,
  }
}
export default connect(mapStateToProps, null)(HalfHalfModal)
