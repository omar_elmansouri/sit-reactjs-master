import React from 'react'
import CloseTag from '../CloseTag'
import HorizontalLine from '../HorizontalLine'
import TickIcon from '../../assets/icons/tick-white.png'
import CashIcon from '../../assets/icons/cash.png'
import { connect } from 'react-redux'
import { withTranslation, WithTranslation } from 'react-i18next'
import * as types from '../../redux/user/types'
import { orderData } from '../../redux/cart/types'
import { orderRequestAction } from '../../redux/cart/actions'
import { FaRegCreditCard } from 'react-icons/fa'
import Loader from 'react-loader-spinner'
import { callInitiatePayment } from '../../redux/orders/service'
import { selectToken } from '../../redux/selectors/selectors'
import { select } from 'redux-saga/effects'
import { Item } from 'semantic-ui-react'
import { CartItem } from '../../redux/cart/types'

interface Props extends WithTranslation {
  visible: boolean
  total?: any
  paymentModel?(): void

  onClose?(): void

  loading: boolean
  paymentMethods: any
  checkoutConfig: any
  cart?: any
  product?: any
}

interface State {
  billingAddress: boolean
}

const bodyInitialState = {
  browserInfo: {
    acceptHeader: 'string',
    colorDepth: 0,
    javaEnabled: true,
    javaScriptEnabled: true,
    language: 'string',
    screenHeight: 0,
    screenWidth: 0,
    timeZoneOffset: 0,
    userAgent: 'string',
  },
  origin: 'string',
  paymentMethod: {
    applepayToken: 'string',
    cvc: 'string',
    encryptedCardNumber: 'string',
    encryptedExpiryMonth: 'string',
    encryptedExpiryYear: 'string',
    encryptedSecurityCode: 'string',
    expiryMonth: 'string',
    expiryYear: 'string',
    googlepayToken: 'string',
    holderName: 'string',
    idealIssuer: 'string',
    installmentConfigurationKey: 'string',
    issuer: 'string',
    number: 'string',
    personalDetails: {
      dateOfBirth: 'string',
      firstName: 'string',
      gender: 'MALE',
      infix: 'string',
      lastName: 'string',
      shopperEmail: 'user@user.com',
      socialSecurityNumber: 'string',
      telephoneNumber: 'string',
    },
    recurringDetailReference: 'string',
    sepaIbanNumber: 'string',
    sepaOwnerName: 'string',
    separateDeliveryAddress: true,
    storeDetails: true,
    storedPaymentMethodId: 'string',
    type: 'string',
  },
}

const ordersDTOInitialState = {
  address: 'A24 / 25 Bella Vista',
  currency: 'usd',
  customerName: 'Hello one',
  email: 'user@user.com',
  id: 0,
  items: [
    {
      categoryId: 0,
      id: 0,
      image: 'string',
      imageContentType: 'string',
      information: 'string',
      ingredient: 'string',
      itemName: 'string',
      price: 0,
      taxRateIfDineIn: 0,
      taxRateIfPickUp: 0,
    },
  ],
  orderStatus: 'string',
  orderType: 'string',
  paymentId: 0,
  paymentStatus: 'string',
  phoneNumber: 'string',
  tableId: 0,
  storeId: 0,
  tableNumber: 'string',
  totalAmount: 0,
}

class PaymentCheckoutModel
  extends React.Component <Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      billingAddress: false,
    }
  }

  onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log('e target ', e.target.value)
    this.setState({
      billingAddress: !this.state.billingAddress,
    } as Pick<State, any>)
  }

  onSubmit = () => {
    const {paymentMethods, checkoutConfig} = this.props;
    console.log('API information =>', {paymentMethods, checkoutConfig})
    let token = select(selectToken);
    const ordersDTO = {
      ...ordersDTOInitialState,
      ...this.props.total,
      items:{
        ...this.props.total.cart
      }
    }
    let body = {...bodyInitialState, origin: checkoutConfig.originKey};
    delete ordersDTO.cart;
    console.log({orderDto:ordersDTO})

    callInitiatePayment(body, ordersDTO, token).then(res => console.log({ response: res })).catch(err => console.log({ error: err }))
  }

  render() {
    const { grandTotal , cart } = this.props.total
  
    const {
      t,
      onClose,
      visible,
      checkoutConfig,
      paymentMethods,
    } = this.props
    console.log({ paymentMethods, checkoutConfig })
    const loading = this.props.loading
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body  md'}>
          <div className={'white-header ph'}>
            <CloseTag onClose={onClose}/>
            <p className={'def'}>{t("Payment your order here") }</p>
            <h3 className={'title'}>{
              cart.slice(0,1).map((name: CartItem) => (
              <p>{name.product.category.store.name}</p>
            ))
            }
           
              </h3>
            <p className={'sub'}>  {cart.slice(0,1).map((name: CartItem) => (
                <p>{name.product.category.store.location}</p>
              ))}</p> 
                           
            <HorizontalLine/>
            <div className="customer-form">
              <div className="address">
                <div className="billing-header">
                  <div className="billing-header-title">
            <span className="billing-header-radio">
              <input type="checkbox" checked={this.state.billingAddress} name="enableBilling"
                     onChange={this.onChangeHandler}/>
            </span>
                    <span className="billing-header-title-name"> {t("Enable Billing Information for 3DS2")}</span>
                  </div>
                </div>
                <form className="address-form" action="/destination" method="get">
                  <div className="address-line">
                    <div className="address-input">
                      <label className="address-label" htmlFor="firstName">
                        {t("First Name")}
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder={t("First Name")}
                        name="firstName"
                        disabled={!this.state.billingAddress}
                      />
                    </div>
                    <div className="address-input">
                      <label className="address-label" htmlFor="lastName">
                        {t("Last Name")}
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder={t("Last Name")}
                        name="lastName"
                        disabled={!this.state.billingAddress}
                      />
                    </div>
                  </div>
                  <div className="address-line">
                    <div className="address-input">
                      <label className="address-label" htmlFor="houseNumberOrName">
                        {t("House number")}
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder={t("House number")}
                        name="houseNumberOrName"
                        disabled={!this.state.billingAddress}
                      />
                    </div>
                    <div className="address-input">
                      <label className="address-label" htmlFor="street">
                        {t("Street")}
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder={t("Street")}
                        name="street"
                        disabled={!this.state.billingAddress}
                      />
                    </div>
                  </div>
                  <div className="address-line">
                    <div className="address-input full-width">
                      <label className="address-label" htmlFor="city">
                        {t("City")}
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder={t("City")}
                        name="city"
                        disabled={!this.state.billingAddress}
                      />
                    </div>
                  </div>
                  <div className="address-line">
                    <div className="address-input">
                      <label className="address-label" htmlFor="state">
                        {t("State")}
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder={t("State")}
                        name="stateOrProvince"
                        disabled={!this.state.billingAddress}
                      />
                    </div>
                    <div className="address-input">
                      <label className="address-label" htmlFor="zipcode">
                        {t("Zip Code")}
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder={t("Zip Code")}
                        name="postalCode"
                        disabled={!this.state.billingAddress}
                      />
                    </div>
                    <div className="address-input">
                      <label className="address-label" htmlFor="country">
                        {t("Country")}
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder={t("Country")}
                        name="country"
                        disabled={!this.state.billingAddress}
                      />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className={'footer'}>
            <div className={'right'} onClick={this.onSubmit}>
              {loading ? <div style={{ margin: '10px' }}><Loader
                  type="Oval"
                  color="white"
                  height={18}
                  width={18}
                  timeout={700000}
                /></div> :
                <h3>{t("Proceed")}</h3>
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const comp = connect((state: any) => {
  return {
    total: state.cart,
    userInfo: state.auth.userInfo,
    loading: state.cart.loading,
    isOrderSubmit: state.cart.isOrderSubmit,
    paymentMethods: state.order.PaymentMethods,
    checkoutConfig: state.order.CheckOutConfig,
  }
}, (dispatch: any) => {
  return {
    oderRequest: (totalData: orderData) => dispatch(orderRequestAction(totalData)),
  }
})(PaymentCheckoutModel)

export default withTranslation()(comp)
