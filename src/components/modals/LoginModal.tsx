import React from 'react'
import ProfileIcon from '../../assets/icons/profile.png'
import CloseTag from '../CloseTag'
import EmailIcon from '../../assets/icons/person.png'
import PasswordIcon from '../../assets/icons/password.png'
import { connect } from 'react-redux'
import { User } from '../../redux/user/types'
import { loginUser } from '../../redux/user/actions'
import { remove } from '../../redux/user/actions'
import { withTranslation, WithTranslation } from 'react-i18next'
import Loader from 'react-loader-spinner'

interface Props extends WithTranslation {
  onClose?(): void

  visible: boolean

  onDontHaveAccount?(): void

  onLogin?(): void

  onError?(errorMessage: string): void

  auth: any;
  Signin: (user: User) => void
  removeError: any
}

interface State {
  username: string,
  password: string,
  formErrors: any[]
}

class LoginModal extends React.Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      username: '',
      password: '',
      formErrors: [],
    }
  }

  onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [e.target.name]: e.target.value,
      formErrorMessage: '',
    } as Pick<State, any>)
    this.props.removeError()
  }
  loginHandler = () => {
    const { username, password } = this.state
    const { errorMessage } = this.props.auth
    const { t } = this.props
    const formErrors:any[] = [];
    if (username === '') formErrors.push('username')
    if (password === '') formErrors.push('username')
    if(formErrors.length===0)
    {
      const user = {
        username,
        password,
      }
      this.props.Signin(user)
      {
        errorMessage &&
        this.props.onError(errorMessage)
      }
    }
    this.setState({formErrors});
  }

  formErrorMessage = (field) => {
    if (this.state.formErrors.length > 0 && this.state.formErrors.includes(field)) {
      return (
        <h4 className={'alert-text'}>
          This field is required.
        </h4>
      )
    }
  }

  render() {
    const { t } = this.props
    const { onClose, visible, onDontHaveAccount } = this.props
    const { errorMessage, loading } = this.props.auth
    const { username, password } = this.state
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body'}>
          <div className={'header'}>
            <CloseTag onClose={onClose}/>
            <img src={ProfileIcon} alt={'Profile'} className={'main-icon'}/>
            <h3 className={'title'}>{t('LOGIN TO YOUR ACCOUNT')}</h3>
          </div>
          <div className={'content'}>
            <div className="field-container">
              <img src={EmailIcon} alt="Email"/>
              <div>
                {this.formErrorMessage('username')}
              <input type="email" name="username" value={username} onChange={this.onChangeHandler}
                     placeholder={t('USER NAME')} className={'no-margin'}/>
              </div>
            </div>
            <div className="field-container">
              <img src={PasswordIcon} alt="Password"/>
              <div>
                {this.formErrorMessage('password')}
              <input type="password" name="password" value={password} onChange={this.onChangeHandler}
                     placeholder={t('PASSWORD')} className={'no-margin'}/>
              </div>
            </div>
            <div className={'hr'}/>
            <div className={'button'} onClick={this.loginHandler} >
              {loading ? <div className={'loader-login'}><Loader
                  type="Oval"
                  color="white"
                  height={18}
                  width={18}
                  timeout={700000}
                /></div> :
                <>{t('LOGIN')} </>
              }
            </div>
            <div className={'sub-link center'} onClick={onDontHaveAccount}>
              <em>{t('Don\'t have an account')}</em>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const comp = connect((state: any) => {
  return {
    auth: state.auth,
  }
}, (dispatch: any) => {
  return {
    Signin: (login: User) => dispatch(loginUser(login)),
    removeError: () => dispatch(remove()),
  }
})(LoginModal)

export default withTranslation()(comp)
