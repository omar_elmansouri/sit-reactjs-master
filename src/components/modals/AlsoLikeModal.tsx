import React from 'react'
import CloseTag from '../CloseTag'
import HorizontalLine from '../HorizontalLine'
import Option from '../../interfaces/Option'
import PlusIcon from '../../assets/icons/plus.png'
import MinusIcon from '../../assets/icons/minus.png'
import VerticalLine from '../VerticalLine'
import { CartItem, total } from '../../redux/cart/types'
import { Product } from '../../redux/products/types'
import SizeOptionCheckbox from '../SizeOptionCheckbox'
import RightIcon from '../../assets/icons/right-icon.png'
import { connect } from 'react-redux'
import { grandTotalAction } from '../../redux/cart/actions'
import { withTranslation, WithTranslation } from "react-i18next";

interface Props extends WithTranslation {
  data?: any;
  cart?: any,
  GrandTotalSave?: any,

  onClose?(): void

  onCustomerLogin?(): void

  onCheckoutClick?(): void

  onProductClick(product: Product): void

  visible: boolean
  auth?: any
}

class AlsoLikeModal extends React.Component<Props> {
  render(): React.ReactNode {
    const {t}=this.props
    const {
      onClose,
      visible,
      onProductClick,
    } = this.props

    const onContinue = () => {
      const { cart } = this.props
      const { loggedInStatus } = this.props.auth
      const itemId = cart.cart.map((cartItem: CartItem) => {
        return cartItem.product
      })

      let grandTotal = cart.cart.map((cartItem: CartItem) => {
        return cartItem.quantity * (cartItem.product.price + cartItem.totalPrice)
      })
      let GrandTotal = 0
      for (let i = 0; i < grandTotal.length; i++) {
        GrandTotal += grandTotal[i]
      }

      const IdItems = itemId.map((items: Product) => {
        return {
          id: items.id,
        }
      })
      let totalData = {
        grandTotal: GrandTotal,
        items: IdItems,
      }

      onClose()
      this.props.GrandTotalSave(totalData)
      if (loggedInStatus === true) {
        this.props.onCheckoutClick()
      } else {
        this.props.onCustomerLogin()
      }
    }
    return (
      <div className={`modal also ${visible ? 'open' : ''}`}>
        <div className={'body  md'}>
          <div className={'header ph'}>
            <CloseTag onClose={onClose}/>
            <h3 className={'title'}>YOU MIGHT ALSO LIKE?</h3>
          </div>
          <div className={'content also-list'}>
            {this.props.data.length !== 0 && this.props.data.find(item => item && item.name === 'Deserts') && this.props.data.find(item => item && item.name === 'Deserts').items.map((product, index) => product && (
              <div className={'product-item also-item'} key={index} onClick={() => onProductClick(product)}>
                <img
                  src={`data:${product.imageContentType};base64,${product.image}`}
                  alt=""
                  sizes={'700vm'}
                  className={'also-product-image'}
                />
                <div className={'product-info also-info'}>
                  <h4 className={'name'}>{product.itemName}</h4>
                  <div className={'bottom'}>
                    <p>${product.price.toFixed(2)}</p>
                    <img src={RightIcon} alt="Right icon"/>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className={'footer'}>
            <div className={'right'} onClick={onContinue}>
              <h3>{t('CONTINUE TO CHECKOUT')}</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    cart: state.cart,
    auth: state.auth,
    data: state.products.category,
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    GrandTotalSave: (totalData: total) => dispatch(grandTotalAction(totalData)),
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(AlsoLikeModal))
