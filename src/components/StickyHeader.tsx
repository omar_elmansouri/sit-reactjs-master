import React from 'react'
import { connect } from 'react-redux'
import LogoImg from '../assets/images/logo.png'
import TopBannerImg from '../assets/images/topbanner.jpg'
import CartIcon from '../assets/icons/cart.png'
import HamburgerIcon from '../assets/icons/hamburger.png'
import { BrowserRouter as Router, Link } from 'react-router-dom'
import { withTranslation, WithTranslation } from "react-i18next";
import { Dropdown } from 'semantic-ui-react'
import ProfileIcon from '../assets/icons/person-orange.png'

interface Props extends WithTranslation{
  onRegister?(): void
  onLogin?(): void
  onCategoryToggle?(): void
  onCartToggle?(): void
  userToken?: any
  onAccount?(): void

}

interface State {
  value: string
}

class StickyHeader extends React.Component<Props ,State>{
  constructor(props) {
    super(props)
    this.state = {
      value: "en"
    }
  }

  onLanguageHandle = (event) => {
    let newLang = event.target.value;
    this.setState({ value: newLang })
    this.props.i18n.changeLanguage(newLang)
  }

  renderRadioButtons = () => {
    return (
      <div>
        <select className="select-language-dropdown" name="language" onChange={(e) => this.onLanguageHandle(e)}>
          <option lang="en" value="en" selected>English</option>
          <option lang="fr" value="fr">French</option>
        </select>
      </div>
    )
  }
  render(): React.ReactNode {
    const { onRegister, onLogin, onCategoryToggle, onCartToggle, onAccount} = this.props
    const { t } = this.props
    return (
      <header id={'top-banner'} style={{position: 'fixed' , width: '100%' , backgroundColor: 'black' , height: '0px' ,padding: '10px 44px 50px 0px' }}>
        <div className={'logo-container'}>
          <div className={'category-btn'} onClick={onCategoryToggle}>
            <img src={HamburgerIcon} alt="" />
          </div>
          <Link to={'/'}>
            <img src={LogoImg} className={'logoStickyNav'} alt={'Logo image'} />
          </Link>
        </div>
        <div className={'nav-bar'}>
          <div>
            {this.renderRadioButtons()}
          </div>
          {this.props.userToken ? (
            <div className={'auth-links-myaccount'}>
              <Link className={'auth-login'} to={'/account'}>
                <img src={ProfileIcon} alt={'Profile'} className="icon-myaccount"/>  MY ACCOUNT
              </Link>
            </div>
          ) : (
            <div className={'auth-links'}>

              <button className={'auth-login fill'} onClick={onLogin}>
                {t('LOGIN')}
              </button>
              <button className={'auth-sign-up'} onClick={onRegister}>
                {t("SIGNUP")}
              </button>
            </div>
          )}

          <div className={'order-btn'} onClick={onCartToggle}>
            <img src={CartIcon} alt="" />
          </div>
        </div>
      </header>
    )
  }
}

const comp = connect((state: any) => {
  return {
    userToken: state.auth.token || false,
  };
})(StickyHeader);

export default withTranslation()(comp);