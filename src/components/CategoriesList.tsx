import React from 'react'
import CloseTag from './CloseTag'
import LogoImage from '../assets/images/logo.png'
import { connect } from 'react-redux'
import { Category } from '../redux/products/types'
import { Link} from 'react-scroll'

interface Props {
  data?: Category[]
  onCategoryPress(category: Category): void
  slideVisible?: boolean
  onSlideToggle?(): void
}

class CategoriesList extends React.Component<Props> {
  render(): React.ReactNode {
    let categories = this.props.data || []
    const { onCategoryPress, slideVisible, onSlideToggle } = this.props
    return (
      <div style={{height: '60px', position:'absolute'}} className={`categories-list ${slideVisible ? 'open' : ''}`}>
       Bonjour
      </div>
    )
  }
}

const mapStateToProps = (state: any): any => {
  return {
    data: state.products.category,
  }
}

export default connect(mapStateToProps, null)(CategoriesList)