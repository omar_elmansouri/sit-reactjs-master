import React from 'react'
import PersonIcon from '../../assets/icons/person.png'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { startNewOrderAction } from '../../redux/cart/actions'
import { withTranslation, WithTranslation } from "react-i18next";

interface Props extends WithTranslation {
  onClose(): void,
  onRegister(): void,
  onLogin(): void,
  newOrder?: any
  visible: boolean
  userToken?: any
}

class LeftSideBar extends React.Component<Props> {
  handleStartNew = () => {
    this.props.newOrder()
    this.props.onClose()
  }
  render(): React.ReactNode {
    const { onClose, visible, onLogin, onRegister } = this.props
    return (
      <section className={`sidebar sidebar-left ${visible ? 'open' : ''}`}>
        <div className="content">
          {this.props.userToken ? (
            <div className={'top'}>
              <img src={PersonIcon} />
              <p>User</p>
              <Link to={'/account'}><p>My Account</p>
              </Link>
            </div>
          ) :
            <div className={'top'}>
              <div className={'auth-links'}>
                <button className={'auth-login fill'} onClick={onLogin}>
                  LOGIN
                </button>
                <button className={'auth-sign-up'} onClick={onRegister}>
                  SIGN UP
                </button>
              </div>
            </div>}
          <div className={'option'} onClick={this.handleStartNew}>
            <p>Restart Order</p>
          </div>
        </div>
      </section>
    )
  }
}


const comp = connect((state: any) => {
  return {
    userToken: state.auth.token || false,
  };
}, (dispatch: any) => {
  return {
    newOrder: () => dispatch(startNewOrderAction()),

  };
})(LeftSideBar);

export default withTranslation()(comp);

