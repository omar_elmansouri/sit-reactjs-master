import React from 'react'
import HamburgerIcon from '../../assets/icons/hamburger-no-padding.png'
import CrossIcon from '../../assets/icons/cross-white.png'
import SearchIcon from '../../assets/icons/search-white.png'

interface Props {
  leftBar: boolean,
  onLeftPress(): void,
  onRightPress(): void,
}
class HeaderMobile extends React.Component<Props> {
  render(): React.ReactNode {
    const { onLeftPress, onRightPress } = this.props
    return (
      <header id={'top-banner-mobile'}>
        <img alt={'Menu'} src={!this.props.leftBar?HamburgerIcon:CrossIcon} style={{ width: 20, height: 20, cursor: 'pointer' }} onClick={onLeftPress}/>
        <p>Atlassimagroup</p>
        <img alt={'Search'} src={SearchIcon} style={{ width: 16, height: 16, cursor: 'pointer' }} onClick={onRightPress}/>
      </header>
    )
  }
}

export default HeaderMobile
