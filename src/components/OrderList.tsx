import React from 'react'
import CloseTag from './CloseTag'
import { connect } from 'react-redux'
import { CartItem } from '../redux/cart/types'
import { removeProductFromCart, updateProductInCart } from '../redux/cart/actions'
import { Product } from '../redux/products/types'
import OrderListContent from './OrderListContent'
import CustomerDetailModel from './modals/CustomerDetailModel'

interface Props {
  slideVisible: boolean
  cart?: any
  onSlideToggle?(): void
  onAlsoLikeClick?(): void
  onReceiptClick(product: Product): void
  updateProductInCart?(cartItem: CartItem): void
  removeProductFromCart?(cartItem: CartItem): void
}

class OrderList extends React.Component<Props> {
  state = {
    quantity: 1,
  }
  render(): React.ReactNode {
    const { slideVisible, onSlideToggle, onReceiptClick, onAlsoLikeClick } = this.props
    return (
      <div className={`order ${slideVisible ? 'open' : ''}`}>
        {slideVisible && (
          <CloseTag onClose={onSlideToggle}/>
        )}
        <OrderListContent onReceiptClick={onReceiptClick}
                          onAlsoLikeClick={onAlsoLikeClick}/>
      </div>
    )
  }
}

const mapStateToProps = (state: any): any => {
  return {
    cart: state.cart,
  }
}

const mapDispatchToProps = (dispatch: any): any => {
  return {
    updateProductInCart: (cartItem: CartItem) => dispatch(updateProductInCart(cartItem)),
    removeProductFromCart: (cartItem: CartItem) => dispatch(removeProductFromCart(cartItem.id || 0)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderList)
