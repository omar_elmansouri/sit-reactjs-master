import React from 'react'
import { connect } from 'react-redux'
import LogoImg from '../assets/images/logo.png'
import TopBannerImg from '../assets/images/topbanner.jpg'
import CartIcon from '../assets/icons/cart.png'
import HamburgerIcon from '../assets/icons/hamburger.png'
import { Link } from 'react-router-dom'
import { withTranslation, WithTranslation } from "react-i18next";
import ProfileIcon from '../assets/icons/person-orange.png'

interface Props extends WithTranslation{
  onRegister?(): void

  onLogin?(): void

  onCategoryToggle?(): void

  onCartToggle?(): void

  userToken?: any

  onAccount?(): void


}

interface State {
	value: string
}

class Header extends React.Component<Props ,State>{
  constructor(props) {
    super(props)
    this.state = {
      value: "en"
    }
  }

  onLanguageHandle = (event) => {
    let newLang = event.target.value;
    this.setState({ value: newLang })
    this.props.i18n.changeLanguage(newLang)
  }

  renderRadioButtons = () => {
    return (
      <div>
        <select className="select-language-dropdown" name="language" onChange={(e) => this.onLanguageHandle(e)}>
          <option lang="en" value="en" selected>English</option>
          <option lang="fr" value="fr">French</option>
        </select>
      </div>
      // <div><input
      //   checked={this.state.value === 'en'}
      //   name="language" onChange={(e) => this.onLanguageHandle(e)} value="en" type="radio" />English &nbsp;
      //   <input name="language" value="ur"
      //          checked={this.state.value === 'ur'}
      //          type="radio" onChange={(e) => this.onLanguageHandle(e)} />Japanese</div>
    )
  }
  render(): React.ReactNode {
    const { onRegister, onLogin, onCategoryToggle, onCartToggle, onAccount} = this.props
    const { t } = this.props
    return (

      <header id={'top-banner'} style={styles.topBanner}>
        <div className={'logo-container'}>
          <div className={'category-btn'} onClick={onCategoryToggle}>
            <img src={HamburgerIcon} alt="" />
          </div>
          <Link to={'/'}>
            <img src={LogoImg} className={'logo'} alt={'Logo image'} />
          </Link>
        </div>
        <div className={'nav-bar'}>
          <div>
            {this.renderRadioButtons()}
          </div>
          {this.props.userToken ? (
            <div className={'auth-links-myaccount'}>
              <Link className={'auth-login'} to={'/account'}>
                <img src={ProfileIcon} alt={'Profile'} className="icon-myaccount"/>  MY ACCOUNT
              </Link>
            </div>
          ) : (
              <div className={'auth-links'}>

                <button className={'auth-login fill'} onClick={onLogin}>
                  {t('LOGIN')}
                </button>
                <button className={'auth-sign-up'} onClick={onRegister}>
                  {t("SIGNUP")}
              </button>
              </div>
            )}

          <div className={'order-btn'} onClick={onCartToggle}>
            <img src={CartIcon} alt="" />
          </div>
        </div>
      </header>
    )
  }
}


// const mapStateToProps = (state: any): any => {
//   return {
//     userToken: state.auth.token || false,
//   }
// }

const comp = connect((state: any) => {
  return {
    userToken: state.auth.token || false,
  };
})(Header);

export default withTranslation()(comp);

const styles = {
  topBanner: {
    
  },
}
